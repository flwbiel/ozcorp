package br.com.ozcorp;

public class Departamento {

	String nome;
	String sigla;
	Cargo Cargo;
	
	public Departamento(String nome, String sigla, Cargo cargo) {
		super();
		this.nome = nome;
		this.sigla = sigla;
		this.Cargo = cargo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Cargo getCargo() {
		return Cargo;
	}

	public void setCargo(Cargo cargo) {
		Cargo = cargo;
	}
	
	
}
