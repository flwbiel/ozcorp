package br.com.ozcorp;

public enum TipoSanguineo {
	
	A_POSITIVO, A_NEGATIVO, AB_POSITIVO, B_POSITIVO, B_NEGATIVO, AB_NEGATIVO, O_POSITIVO, O_NEGATIVO;

}
