package br.com.ozcorp;

public class Cargo {

	String titulo;
	double salarioBase;
	public Cargo(String titulo, double salarioBase) {
		super();
		this.titulo = titulo;
		this.salarioBase = salarioBase;
		
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public double getSalarioBase() {
		return salarioBase;
	}
	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}
	
	
}
