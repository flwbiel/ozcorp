package br.com.ozcorp;

public enum Sexo {
	MASCULINO("Masculino"), FEMININO("Feminino"), OUTRO("Outro");
	
	String nome;
	Sexo(String nome){
		this.nome = nome;
	}

}
