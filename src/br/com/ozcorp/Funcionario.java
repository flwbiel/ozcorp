package br.com.ozcorp;

public class Funcionario {
	
	String nome;
	String rg;
	String cpf;
	String matricula;
	String email;
	String senha;
	TipoSanguineo TipoSanguineo;
	Sexo Sexo;
	int nivelAcesso;
	Departamento Departamento;
	
	public Funcionario(String nome, String rg, String cpf, String matricula, String email, String senha,
			br.com.ozcorp.TipoSanguineo tipoSanguineo, br.com.ozcorp.Sexo sexo, int nivelAcesso,
			br.com.ozcorp.Departamento departamento) {
		super();
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.matricula = matricula;
		this.email = email;
		this.senha = senha;
		TipoSanguineo = tipoSanguineo;
		Sexo = sexo;
		this.nivelAcesso = nivelAcesso;
		Departamento = departamento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public TipoSanguineo getTipoSanguineo() {
		return TipoSanguineo;
	}

	public void setTipoSanguineo(TipoSanguineo tipoSanguineo) {
		TipoSanguineo = tipoSanguineo;
	}

	public Sexo getSexo() {
		return Sexo;
	}

	public void setSexo(Sexo sexo) {
		Sexo = sexo;
	}

	public int getNivelAcesso() {
		return nivelAcesso;
	}

	public void setNivelAcesso(int nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}

	public Departamento getDepartamento() {
		return Departamento;
	}

	public void setDepartamento(Departamento departamento) {
		Departamento = departamento;
	}

}
